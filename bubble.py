#!/usr/bin/env python
# -*- coding: utf-8 -*-

from connectivity import writeVTK

import numpy as np
import argparse
from ConfigParser import ConfigParser

parser = argparse.ArgumentParser(description='INI and VTK generator a single bubble')

parser.add_argument('-N' , action="store", dest="N" , type = int)
parser.add_argument('-R' , action="store", dest="R" , type = float)
parser.add_argument('-a' , action="store", dest="a" , type = float)
parser.add_argument('-b' , action="store", dest="b" , type = float)
parser.add_argument('-red',action="store", dest="red" , type = float)
parser.add_argument('-blue',action="store", dest="blue" , type = float)

tau = 1.0

prm = parser.parse_args()

dst = lambda r,rc : np.sqrt( (r[0] - rc[0])**2 + (r[1] - rc[1])**2 + (r[2] - rc[2])**2 ) 

bubble = lambda r,rc, R , beta : 0.5* (1 + np.tanh( beta * (R - dst(r,rc)) ) ) 

rho = 1.0
finalStep = 10000

Nx , Ny , Nz = prm.N, prm.N, prm.N

center = (Nx/2,Ny/2,Nz/2)

R = prm.R
beta = prm.b

wR = np.array( [ [ [ bubble( (x,y,z) , center , R , beta ) for z in range(Nx) ] for y in range(Ny) ] for x in range(Nz) ] , dtype = np.float32)

geo =  np.ones( (Nx,Ny,Nz) , dtype = np.float32 )

geoFile = "bubble_geo.vtk"
redFile = "bubble_rhoR.vtk"
blueFile =  "bubble_rhoB.vtk"

writeVTK( geo , "bubble_geo.vtk" , filetype = "ASCII" )
writeVTK( prm.red * wR        , "bubble_rhoR.vtk" )
writeVTK( prm.blue * (1.0-wR) , "bubble_rhoB.vtk" )

cfgfile = open("bubble.ini","w")

cfgfile.write("# Configuration file for the GRAD-RESEARCH LBM Algorithm \n\
# Project: A simple bubble \n\
# Author: Diogo Nardelli Siebert \n\
# Data: 18 / 08 / 2015 \n\n" )

Config = ConfigParser()
Config.add_section('General')
Config.set('General','tau',tau)
Config.set('General','number_of_steps', finalStep )

Config.add_section('Geometry')
Config.set('Geometry','filetype',"vtk")
Config.set('Geometry','filename',geoFile)
Config.set('Geometry','lattice_spacing',1.0)

Config.add_section('Output')
Config.set('Output','write_zero_step',"true")
Config.set('Output','number_of_files', finalStep / 1000 )
Config.set('Output','binary',"true")
Config.set('Output','density',"true")
Config.set('Output','velocity',"true")

Config.add_section('Initial_Condition')
Config.set('Initial_Condition','use_files',"true")
Config.set('Initial_Condition','red_file', redFile )
Config.set('Initial_Condition','blue_file', blueFile)

Config.add_section('SHC')
Config.set('SHC',"alpha",prm.a)
Config.set('SHC','beta',prm.b)
Config.set('SHC','wall_concentration',0.6)
Config.write(cfgfile)

cfgfile.close()



