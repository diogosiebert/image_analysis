# -*- coding: utf-8 -*-
"""
Created on Thu May 21 10:13:03 2015

@author: diogo
"""

import numpy as np
import scipy.ndimage.measurements as im
from vtk.util.numpy_support import vtk_to_numpy , numpy_to_vtk
from vtk import vtkFloatArray, vtkIntArray, vtkCharArray , vtkDataSetReader, vtkImageData, vtkDataSetWriter, VTK_FLOAT, VTK_INT , VTK_CHAR, vtkVersion
import csv
import locale

locale.setlocale(locale.LC_NUMERIC, "C.UTF-8" )

def readCSV(filename, header = True, separator = " "):
    f = open(filename,"r")
    reader = csv.reader(f, delimiter = separator)

    if (header): reader.next()

    data = []    

    nrows = 0
    for row in reader:
        nrows += 1
        ncols = 0
        for col in row:
            ncols += 1 
            data.append( float(col) )

    f.close()
    return np.array( data ).reshape( [  nrows, ncols] ).transpose()

def readVTK(filename):
    reader = vtkDataSetReader()
    reader.SetFileName(filename)
    reader.Update()
    vtkSP = reader.GetOutput()
    dim = vtkSP.GetDimensions() 
    if ( reader.GetNumberOfScalarsInFile() ):
        data = vtk_to_numpy( vtkSP.GetPointData().GetScalars() )
        data = data.reshape(dim[2], dim[1], dim[0])
        data = data.transpose( (2,1,0) )
        return data
    elif ( reader.GetNumberOfVectorsInFile() ):
        data = vtk_to_numpy(vtkSP.GetPointData().GetVectors())
        data = data.reshape(dim[2], dim[1], dim[0], 3 )
        data = data.transpose( (3, 2 ,1, 0) )
        return data

def loadraw(filename, size , data = np.uint8, discard = 0):
    return np.transpose( np.fromfile(filename, dtype = data )[discard:].reshape([ size[2], size[1], size[0] ]) , axes = (2,1,0) )

def duplicate(A, axis = 0):
    if   (axis == 0): return np.concatenate( (A, A[::-1,:,:]) , axis = 0)
    elif (axis == 1): return np.concatenate( (A, A[:,::-1,:]) , axis = 1)
    elif (axis == 2): return np.concatenate( (A, A[:,:,::-1]) , axis = 2)            
    
def porosity(a):
    return np.sum(a) / np.size(a)

if (vtkVersion.GetVTKMajorVersion() < 6):
    def writeVTK(A,filename,filetype="Binary"):
       
        id = vtkImageData()
        
        if (A.dtype == np.float32):
            id.SetScalarTypeToFloat()
            vtkdata = numpy_to_vtk( (A.transpose(2,1,0)).flatten() , deep = True, array_type = vtkFloatArray().GetDataType())
        elif (A.dtype == np.int32):
            id.SetScalarTypeToInt()
            vtkdata = numpy_to_vtk( (A.transpose(2,1,0)).flatten() , deep = True , array_type = vtkIntArray().GetDataType())
        elif (A.dtype == np.int8):
            id.SetScalarTypeToChar()
            vtkdata = numpy_to_vtk( (A.transpose(2,1,0)).flatten() , deep = True , array_type = vtkCharArray().GetDataType() )
       
        id.GetPointData().SetScalars( vtkdata )
        id.SetDimensions( A.shape )
        id.Update()
        
        writer = vtkDataSetWriter()
        if (filetype == "ASCII"): writer.SetFileTypeToASCII()
        else:
            writer.SetFileTypeToBinary()
            
        writer.SetInput( id )
        writer.SetFileName( filename )
        writer.Write()
else:
    def writeVTK(A,filename,filetype="Binary"):
       
        id = vtkImageData()
                          
        if (A.dtype == np.float32): dataType = VTK_FLOAT
        elif (A.dtype == np.int32): dataType = VTK_INT
        elif (A.dtype == np.uint8): dataType = VTK_CHAR
            
        id.AllocateScalars( dataType, 1 )
        vtkdata = numpy_to_vtk( (A.transpose(2,1,0)).flatten() , deep = True, array_type = dataType )
       
        id.GetPointData().SetScalars( vtkdata )
        id.SetDimensions( A.shape )
    #    id.Update()
        
        writer = vtkDataSetWriter()
        if (filetype == "ASCII"): writer.SetFileTypeToASCII()
        else:
            writer.SetFileTypeToBinary()
            
        writer.SetInputData( id )
        writer.SetFileName( filename )
        writer.Write()
    
#def writeVTK(s,filename):
#    NX, NY, NZ = s.shape
#    arq=open(filename, 'w')
#    arq.write('# vtk DataFile Version 2.0' + "\n"+ 'Geometria'+"\n"+ 'ASCII' + "\n" + 'DATASET STRUCTURED_POINTS'+"\n"+ 'DIMENSIONS ' + str(NX) + ' ' + str(NY) + ' ' + str(NZ) +  "\n"  +  'ASPECT_RATIO 1 1 1' +  "\n"  + 'ORIGIN 0 0 0' +  "\n" + 'POINT_DATA ' + str(NX*NY*NZ) + "\n"+ 'SCALARS geometria int'+ "\n" +'LOOKUP_TABLE default'+"\n")
#    np.transpose(s, axes= (2,1,0) ).tofile(arq,sep=" ")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
#    arq.close()

def writeRAW(M,filename):
    f = open(filename,"w")
    np.transpose(M, axes = (2,1,0) ).tofile(f)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
    f.close()
    
def is_connected(sample):
    sample_label, num_labels = im.label(sample)
    beg_x = set( sample_label[0,:,:].flatten() )
    end_x = set( sample_label[-1,:,:].flatten() )
    beg_x.remove(0)
    end_x.remove(0)

    beg_y = set( sample_label[:,0,:].flatten() )
    end_y = set( sample_label[:,-1,:].flatten() )
    beg_y.remove(0)
    end_y.remove(0)     

    beg_z = set( sample_label[:,:,0].flatten() )
    end_z = set( sample_label[:,:,-1].flatten() )
    beg_z.remove(0)
    end_z.remove(0)     

    return bool(beg_x.intersection(end_x)), bool(beg_y.intersection(end_y)) , bool(beg_z.intersection(end_z))

# sample_label, num_labels = im.label(sample)

# writeVTK(sample_label,"label.vtk")

