# -*- coding: utf-8 -*-
"""
Created on Tue Jun  9 19:53:55 2015

@author: diogo
"""

import math
import cairo
import numpy
import pickle
from skimage.draw import circle, polygon
from skimage.morphology import binary_dilation, ball
from connectivity import writeVTK

class Throat:
    begin = 0
    end = 0
    r = 5.0 / 1000.
    def __init__(self, b, e, R = 2.0 ):
        self.begin = b
        self.end = e
        self.r = R
        
    def __init__(self, x1 , y1, x2, y2, R ):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.begin = -1
        self.end = -1
        self.r = R
    
    def get_end_position(self):
        return numpy.array([self.x2, self.y2])
        
    def get_beg_position(self):
        return numpy.array([self.x1, self.y1])
        
    def exists(self , throat_lst  ):
        p1 = [ self.begin, self.end]
        if (p1[0] == -1): return 0
        for throat in throat_lst:
            p2 = [ throat.begin, throat.end ]
            if ( p1 == p2 ) or ( p1 == p2[::-1]):
                return 1
        return 0
    def points(self):   
        return [ self.begin , self.end]

class Pore:
    x = 0
    y = 0
    r = 1
    def __init__(self, X , Y , R):
        self.x = X
        self.y = Y
        self.r = R
        
    def overlap(self, other_lst):
        for other in other_lst:
            sqr_dist = (self.x - other.x)**2 + (self.y - other.y)**2 
            sqr_sumr = (self.r + other.r)**2
            if (sqr_dist < sqr_sumr):
                return 1          
        return 0
    
    def get_position(self):
        return numpy.array( [self.x,self.y] )
    def get_radius(self):
        return self.r

class PorousMedia:
    border_x = 0
    border_y = 0
    lx = 1  #mm
    ly = 1  #mm
    plist = []
    tlist = []
    num_pores = 0
    num_throats = 0
    def __init__(self,lx,ly, depth = 1):
        self.depth = depth
        self.lx = lx
        self.ly = ly
    def append_pore(self, p ):
        if (p.overlap(self.plist) == 0 ):
            (self.plist).append( p )
            self.num_pores = len( self.plist )
    def append_throat(self, t):
        if ( ~t.exists(self.tlist) ):
            self.tlist.append( t )
            self.num_throats = len( self.tlist )
    def get_pore(self,i):
        return self.plist[i]
    def get_throat(self,i):
        return self.tlist[i]
    def get_throats(self):
        return self.tlist
    def get_pores(self):
        return self.plist
    def get_throat_position(self,i):
        t = self.tlist[i]
        pb = self.get_pore(t.begin)
        pe = self.get_pore(t.end)        
        return pb.get_position() + pe.get_position()
    def save(self,filename):
        f = open(filename,"wb")
        pickle.dump([ self.plist , self.tlist], f )
        f.close()       
    def read(self,filename):
        f = open(filename,"r")
        [ self.plist, self.tlist ] =  pickle.load(f)
        self.num_throats = len( self.tlist )
        self.num_pores = len( self.plist )
        f.close()
        
# Criana uma nova classe Context para incorporar funções
# para o desenho dos circulos e dos poros.

class NewContext(cairo.Context):
    def draw_pore(self,x0,y0,R):
        self.arc(x0, y0, R ,0 , 2 * math.pi)
    def draw_throat(self,x0,y0,x1,y1,r):
        nx  , ny =  - (y1-y0) ,  (x1 - x0)
        norm = numpy.sqrt( nx**2 + ny**2 )
        nx = nx / norm
        ny = ny / norm
        self.move_to(x0+r*nx,y0+r*ny)
        self.line_to(x0-r*nx,y0-r*ny)
        self.line_to(x1-r*nx,y1-r*ny)
        self.line_to(x1+r*nx,y1+r*ny)
        self.close_path()       

# Desenha os poros e gargantas utilizando a biblioteca cairo

def convert_to_array(media, Nx):
    border = 100

    h = media.lx / float(Nx)
    Ny = int( media.ly / h )

    A = numpy.zeros( [Ny+2*border,Nx+2*border], dtype = numpy.uint8 )
   
    for p in media.get_pores():
        rr, cc = circle(border + p.y/h, border + p.x/h, p.r/h )
        A[rr, cc] = 1
    
    for t in media.get_throats():
        x0 =  t.x1/ h
        y0 =  t.y1 /h
        x1 =  t.x2 /h 
        y1 =  t.y2 /h        
        r = t.r /h
        nx  , ny =  - (y1-y0) ,  (x1 - x0)
        norm = numpy.sqrt( nx**2 + ny**2 )
        nx = nx / norm
        ny = ny / norm           
        y_corner = border + numpy.array( [y0+r*ny,y0-r*ny,y1-r*ny,y1+r*ny] )
        x_corner = border + numpy.array( [x0+r*nx,x0-r*nx,x1-r*nx,x1+r*nx] )
        rr, cc =  polygon( y_corner , x_corner  )
        A[rr, cc] = 1

    return A[border:-border,border:-border]

def draw_media_png(media,filename, WIDTH):

    pixel_per_um = float( WIDTH )/ media.lx 
    HEIGHT =  int( media.ly * pixel_per_um )
     
    surface = cairo.ImageSurface (cairo.FORMAT_ARGB32, WIDTH, HEIGHT)
    ctx = NewContext (surface)

    # Define a cor de fundo e pinta o fundo

    ctx.set_source_rgb(256, 256, 256)
    ctx.paint()
    ctx.set_source_rgb(0, 0, 0)    

    for p in media.get_pores():   
        ctx.draw_pore(p.x* pixel_per_um ,p.y * pixel_per_um, p.r * pixel_per_um)
        ctx.fill()

    for t in media.get_throats():
       # [b,e] = t.points() 
        [x1,y1] =  t.get_beg_position()
        [x2,y2] =  t.get_end_position()
        ctx.draw_throat( x1 * pixel_per_um , y1 * pixel_per_um  , x2 * pixel_per_um , y2 * pixel_per_um , t.r * pixel_per_um  )
        ctx.fill()

    surface.write_to_png (filename) 

def draw_media_svg(media,filename ,device = 0, scale = 1.0, r = 250. , l1 = 3000. , l2 = 2000. , h = 6500. , l3 = 0.0):

    WIDTH =  media.lx * scale
    HEIGHT = media.ly * scale
    
    fo = file(filename, 'w')
    
    surface = cairo.SVGSurface (fo, WIDTH, HEIGHT)
    ctx = NewContext (surface)

    # Define a cor de fundo e pinta o fundo

    ctx.set_source_rgb(0, 0, 0)     

    for p in media.get_pores():   
         ctx.draw_pore(p.x  * scale,p.y  * scale, p.r  * scale)
         ctx.fill()
         
    for t in media.get_throats():
        [b,e] = t.points()        
        [x1,y1] =  t.get_beg_position()
        [x2,y2] =  t.get_end_position()
#        [x1,y1] =  media.get_pore(b).get_position()
#        [x2,y2] =  media.get_pore(e).get_position()
        ctx.draw_throat( x1 * scale , y1 * scale , x2 * scale , y2 * scale , t.r  * scale )
        ctx.fill()  
    
    BORDER_X = media.border_x * scale    
    r = r * scale
    l1 = l1 * scale
    l2 = l2 * scale
    l3 = l3 * scale
    h = h * scale    
          
    y_in = HEIGHT/2.0
    x_in = media.border_x*scale - l1 - l2 - l3
    
    y_out = HEIGHT/2.0
    x_out = WIDTH - media.border_x*scale + l1 + l2 + l3

    if (device==1):
        ctx.set_source_rgb(0, 0, 0)
        ctx.arc(x_in , y_in , r , math.pi/2. , -math.pi/2.)
        ctx.line_to(x_in + l1, y_in - r)
        ctx.line_to(x_in + l1 + l2 , y_in - h)
        ctx.line_to(x_in + l1 + l2 + l3 , y_in - h)
        ctx.line_to(x_in + l1 + l2 + l3 , y_in + h)
        ctx.line_to(x_in + l1 + l2 , y_in + h)
        ctx.line_to(x_in + l1, y_in + r)
        ctx.close_path()
        ctx.fill()
        
        ctx.set_source_rgb(0, 0, 0)
        ctx.arc( x_out , y_out , r , -math.pi/2. , math.pi/2.)
        ctx.line_to( x_out - (l1), y_out + r)
        ctx.line_to( x_out - (l1 + l2) , y_out + h)
        ctx.line_to( x_out - (l1 + l2 + l3) , y_out + h)
        ctx.line_to( x_out - (l1 + l2 + l3) , y_out - h)
        ctx.line_to( x_out - (l1 + l2) , y_out - h)
        ctx.line_to( x_out - (l1), y_out - r)
        ctx.close_path()
        ctx.fill()
        
        ctx.set_source_rgb(0, 0, 0)
        ctx.move_to(x_in + l1 + l2 + l3 , y_in - h)
        ctx.line_to(x_in + l1 + l2 + l3 , y_in + h)
        ctx.line_to(x_out - (l1 + l2 + l3) , y_out + h)
        ctx.line_to(x_out - (l1 + l2 + l3) , y_out - h)
        ctx.close_path()
        ctx.fill()        
         
    surface.finish()
    fo.close()

def draw_media_vtk(media,filename, WIDTH):
    pixel_per_um = float( WIDTH ) / media.lx 
    HEIGHT =  int( media.ly * pixel_per_um )
    DEPTH  =  int( media.depth * pixel_per_um )
    A = numpy.zeros( [ WIDTH, HEIGHT, DEPTH + 1] , dtype = numpy.uint8 )
    A[:,:,-1] = convert_to_array(media, WIDTH)
    B = binary_dilation(A, ball(DEPTH - 1) )
    writeVTK(B,filename)
    
