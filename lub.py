#!/usr/bin/env python
# -*- coding: utf-8 -*-

from connectivity import *
import numpy as np
from numpy.linalg import norm
from argparse import ArgumentParser

parser = ArgumentParser(description='Create file for the lubrification problem')
parser.add_argument('-R'  ,  action="store"  , dest="R" , type = float, help='Radius')
parser.add_argument('-S ' ,  action="store"  , dest="S" , type = float, help="Saturation")
args = parser.parse_args()

S = args.S
R = args.R
D = 2*50.5
Nx = int(D+2)
Ny = Nx
Nz = 1
Rw = R * np.sqrt(S)

rho_red = 1.0
rho_blue = 1.0

rc = np.array( [ (Nx-1)/2 , (Ny-1)/2 , (Nz-1)/2 ] )

circle = lambda r : 1 if norm(r-rc) - R < 0 else 0

if (S!=0):
    wr     = lambda r : 0.5*(1+np.tanh( 2*(norm(r-rc)-Rw)))
else:
    wr     = lambda r : 1

M =    np.array( [ [ [ circle( np.array( [x,y,0 ])) for z in range(Nz)  ] for y in range(Ny) ] for x in range(Nx) ] , dtype = np.int8 ) 
Red =  np.array( [ [ [  wr( np.array( [x,y,0 ]))    for z in range(Nz)  ] for y in range(Ny) ] for x in range(Nx) ] , dtype = np.float32 ) 
Blue = np.array( [ [ [ 1-wr( np.array( [x,y,0 ]))    for z in range(Nz) ] for y in range(Ny) ] for x in range(Nx) ] , dtype = np.float32 ) 
 
writeVTK(M,"lub_meio.vtk")
writeVTK(Red,"lub_red.vtk")
writeVTK(Blue,"lub_blue.vtk")