# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 15:03:36 2015

@author: diogo
"""

import numpy as np
from connectivity import *
from ConfigParser import ConfigParser
import argparse

parser = argparse.ArgumentParser(description='INI and VTK generator for the BFS problem')

parser.add_argument('-h1'  , action="store", dest="h1" , type = int)
parser.add_argument('-Re' , action="store", dest="Re", type = float)
parser.add_argument('-tau', action="store", dest="tau",type = float)

results = parser.parse_args()

Re = results.Re
h1 =  results.h1
tau = results.tau

ER = 2.0
h2 = int( h1 / (ER - 1) )

l1 = 20*h1
l2 = 70*h1

Nx = l1 + l2
Ny = h1 + h2 + 2
Nz = 1

nu = (tau - 0.5)/3.0
U = Re*nu/ float(h1)

Tmed = int(200./3. * Ny / U )
Tstb = int(10./3. * Nx / U )
Tfin = Tmed + Tstb

A = np.zeros([Nx,Ny,Nz],dtype = np.uint8 )

A[l1:,1:h1+1,:] = 1
A[:,h1+1:-1,:] = 1

vtkFilename = "BFS.vtk"
iniFilename = "data.ini"

writeVTK(A,vtkFilename)

cfgfile = open("BFS.ini","w")

cfgfile.write("# Configuration file for the GRAD-RESEARCH LBM Algorithm \n\
# File Standard Version: 0.1 \n\
# Author: Diogo Nardelli Siebert  \n\
# Project: Backward Facing Step (Re = " + str(Re) + ") \n\n" )

Config = ConfigParser()
Config.add_section('General')
Config.set('General','tau',tau)
Config.set('General','number_of_steps', Tfin )

Config.add_section('Geometry')
Config.set('Geometry','filetype',"vtk")
Config.set('Geometry','filename',vtkFilename)
Config.set('Geometry','lattice_spacing',1.0)

Config.add_section('Output')
Config.set('Output','number_of_files',20)
Config.set('Output','binary',"true")
Config.set('Output','density',"true")
Config.set('Output','velocity',"true")

Config.add_section('BFS')
Config.set('BFS',"uavg",U)
Config.set('BFS','h1',h1)
Config.set('BFS','h2',h2)
Config.set('BFS','l1',l1)
Config.set('BFS','l2',l2)
Config.set('BFS','tstb',Tstb)
Config.set('BFS','tavg',Tmed)
Config.write(cfgfile)

cfgfile.close()
